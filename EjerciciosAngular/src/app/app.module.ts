import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ContenedorPadreEj1Component } from './1-Ejercicio-ComunicacionComponentes/1-Version/contenedor-padre-ej1/contenedor-padre-ej1.component';
import { ContenedorHijoEj1Component } from './1-Ejercicio-ComunicacionComponentes/1-Version/contenedor-padre-ej1/contenedor-hijo-ej1/contenedor-hijo-ej1.component';
import { ContendorPadreEj2Component } from './1-Ejercicio-ComunicacionComponentes/2-Version/contendor-padre-ej2/contendor-padre-ej2.component';
import { ContenedorHijoEj2Component } from './1-Ejercicio-ComunicacionComponentes/2-Version/contendor-padre-ej2/contenedor-hijo-ej2/contenedor-hijo-ej2.component';
import { SeccionPadreComponent } from './2-Ejercicio-UsoDirectivas/Componentes/seccion-padre/seccion-padre.component';
import { ArticuloComponent } from './2-Ejercicio-UsoDirectivas/Componentes/seccion-padre/articulo/articulo.component';
import { PracticaDirectivaPersonalizadaComponent } from './3-Ejercicio-Directiva-Personalizada/practica-directiva-personalizada/practica-directiva-personalizada.component';
import { DirectivaPersonalizadaDirective } from './3-Ejercicio-Directiva-Personalizada/DirectivaPersonalizada/directiva-personalizada.directive';

@NgModule({
  declarations: [
    AppComponent,
    ContenedorPadreEj1Component,
    ContenedorHijoEj1Component,
    ContendorPadreEj2Component,
    ContenedorHijoEj2Component,
    SeccionPadreComponent,
    ArticuloComponent,
    PracticaDirectivaPersonalizadaComponent,
    DirectivaPersonalizadaDirective
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
