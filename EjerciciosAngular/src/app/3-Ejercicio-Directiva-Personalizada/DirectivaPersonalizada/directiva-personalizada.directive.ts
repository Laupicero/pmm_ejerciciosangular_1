import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appDirectivaPersonalizada]'
})
export class DirectivaPersonalizadaDirective {
  urlImgActual: String;

  constructor(private el: ElementRef) { }

  //Métodos
  @HostListener('mouseenter') onMouseEnter() {
    this.el.nativeElement.style.border = `solid green 4px`;
    this.el.nativeElement.style.content = `url("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTd0xRLlM-wgNmdU9lgbrGuK1283fTW9CZe3Q&usqp=CAU")`;
    console.log(this.el.nativeElement);
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.el.nativeElement.style.border = `none`;
  }
}
