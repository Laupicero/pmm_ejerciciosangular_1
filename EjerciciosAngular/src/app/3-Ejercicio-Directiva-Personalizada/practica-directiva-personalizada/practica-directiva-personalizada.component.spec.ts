import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PracticaDirectivaPersonalizadaComponent } from './practica-directiva-personalizada.component';

describe('PracticaDirectivaPersonalizadaComponent', () => {
  let component: PracticaDirectivaPersonalizadaComponent;
  let fixture: ComponentFixture<PracticaDirectivaPersonalizadaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PracticaDirectivaPersonalizadaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PracticaDirectivaPersonalizadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
