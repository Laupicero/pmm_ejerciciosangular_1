export class Articulo{
    public titulo: String;
    public texto: String;

    //Constructor
    constructor(titulo: String, texto: String){
        this.titulo = titulo;
        this.texto = texto;
    }
}