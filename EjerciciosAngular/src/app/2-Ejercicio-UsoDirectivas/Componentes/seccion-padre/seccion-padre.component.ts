import { Component, OnInit } from '@angular/core';
//import {Articulo} from './../../Modelos/articulo';

@Component({
  selector: 'secciones-padre',
  templateUrl: './seccion-padre.component.html',
  styleUrls: ['./seccion-padre.component.css']
})
// Intentamos usar una clase TS de modelo pero no funcionó
export class SeccionPadreComponent implements OnInit {
  //Número de veces para recorrer el Array y que nos imprima la noticia '100' veces
  numRepeticiones: number[];
  //Para el ngIf
  mostrarArticulo: boolean;

  ngOnInit(): void {
    this.numRepeticiones = Array(100).fill(0).map((x,i)=>i);
    this.mostrarArticulo = true;
  }


  // Método
  ToggleForArticles(){
    this.mostrarArticulo = !this.mostrarArticulo;
  }

}
