import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeccionPadreComponent } from './seccion-padre.component';

describe('SeccionPadreComponent', () => {
  let component: SeccionPadreComponent;
  let fixture: ComponentFixture<SeccionPadreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeccionPadreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeccionPadreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
