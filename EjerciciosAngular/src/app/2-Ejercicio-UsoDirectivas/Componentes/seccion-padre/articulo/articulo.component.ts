import { Component, OnInit } from '@angular/core';
//import {Articulo} from './../../../Modelo/articulo';

@Component({
  selector: 'app-articulo',
  templateUrl: './articulo.component.html',
  styleUrls: ['./articulo.component.css']
})
export class ArticuloComponent implements OnInit {
  public titulo: String;
  public texto: String;
  public bckColorSelected: String;
  //Logica para los botones
  public redPressed: boolean;
  public greenPressed: boolean;

  

  ngOnInit(): void {
    this.titulo = `Life on Mars`;
    this.texto = `A new study published in the journal Science shows definitive evidence of organic matter on the surface of Mars. The data was collected by NASA's nuclear-powered rover Curiosity.
    It confirms earlier findings that the Red Planet once contained carbon-based compounds. These compounds – also called organic molecules – are essential ingredients for life as scientists understand it.`;
    
    //Color por defecto
    this.bckColorSelected = `bg-light`;

    //Valores de los botones por defecto
    this.redPressed = false;
    this.greenPressed = false;
  }


  //-------------
  //  Métodos
   //-------------
   
  //Cambiar a rojo: changeBckToRed(){ this.bckColorSelected = `bg-danger`;  }
  changeBckToRed(){
    this.redPressed = !this.redPressed;
    if(this.redPressed){
      this.bckColorSelected = `bg-danger`;

    }else{
      this.bckColorSelected = `bg-light`;
    }
  }

  //Cambiar a verde: changeBckToGreen(){ this.bckColorSelected = `bg-success`;  }
  changeBckToGreen(){
    this.greenPressed = ! this.greenPressed;
    if(this.greenPressed){
      this.bckColorSelected = `bg-success`;

    }else{
      this.bckColorSelected = `bg-light`;
    }
  }


  /*
  //  Intentamos usar la clase articulo.ts como modelo, pero no funcionó
  //  Importamos la clase 'articulo'
  @Input('articuloPlantilla') articulo: Articulo;

  // Constructor
  constructor(titulo: String, txt: String) { 
    this.articulo.titulo = `Life on Mars`;
    this.articulo.texto = `A new study published in the journal Science shows definitive evidence of organic matter on the surface of Mars. The data was collected by NASA's nuclear-powered rover Curiosity...`;
  }  


  // En 'articulo.component.html':
  <h4 class="card-title">{{articulo.titulo}}</h4>
    <p class="card-text text-primary">{{articulo.texto}}</p>


  // En 'secciones.component.ts':
  art: Articulo;

    //Constructor
    constructor() {
    this.art = new Articulo("Life on Mars", "A new study published in the journal Science shows definitive evidence of organic matter on the surface of Mars. The data was collected by NASA's nuclear-powered rover Curiosity");
  }


  // En 'secciones.component.html'
  <articulo-component *ngFor="let art of numRepeticiones" [articuloPlantilla]="art"></articulo-component>
  */

}
