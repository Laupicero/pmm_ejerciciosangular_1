import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContendorPadreEj2Component } from './contendor-padre-ej2.component';

describe('ContendorPadreEj2Component', () => {
  let component: ContendorPadreEj2Component;
  let fixture: ComponentFixture<ContendorPadreEj2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContendorPadreEj2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContendorPadreEj2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
