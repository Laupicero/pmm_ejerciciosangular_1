import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'contendor-padre-ej2',
  templateUrl: './contendor-padre-ej2.component.html',
  styleUrls: ['./contendor-padre-ej2.component.css']
})
export class ContendorPadreEj2Component implements OnInit {

  colorBck: String;

  ngOnInit(): void {
    this.colorBck = `bg-info`;
  }


  // Método para cambiar de color
  // Se le pasará el nombre de una de las clases integradas de 'bootstrap'
  cambiarColor(color: String){

    if(color === `rojo`){
      this.colorBck = `bg-danger`;
    }

    if(color === `azul`){
      this.colorBck = `bg-info`;
    }

    if(color === `verde`){
      this.colorBck = `bg-success`;
    }
  }
}
