import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContenedorHijoEj2Component } from './contenedor-hijo-ej2.component';

describe('ContenedorHijoEj2Component', () => {
  let component: ContenedorHijoEj2Component;
  let fixture: ComponentFixture<ContenedorHijoEj2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContenedorHijoEj2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContenedorHijoEj2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
