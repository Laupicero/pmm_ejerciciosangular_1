import { Component, Input } from '@angular/core';

@Component({
  selector: 'contenedor-hijo-ej2',
  templateUrl: './contenedor-hijo-ej2.component.html',
  styleUrls: ['./contenedor-hijo-ej2.component.css']
})
export class ContenedorHijoEj2Component{
  @Input() colorFromPadre: String;

}
