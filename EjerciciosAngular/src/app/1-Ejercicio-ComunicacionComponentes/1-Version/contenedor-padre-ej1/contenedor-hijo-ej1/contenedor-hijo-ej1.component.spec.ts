import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContenedorHijoEj1Component } from './contenedor-hijo-ej1.component';

describe('ContenedorHijoEj1Component', () => {
  let component: ContenedorHijoEj1Component;
  let fixture: ComponentFixture<ContenedorHijoEj1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContenedorHijoEj1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContenedorHijoEj1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
