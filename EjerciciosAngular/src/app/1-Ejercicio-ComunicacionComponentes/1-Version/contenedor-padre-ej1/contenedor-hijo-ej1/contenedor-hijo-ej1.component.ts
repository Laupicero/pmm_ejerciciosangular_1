import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'contenedorHijo-ej1',
  templateUrl: './contenedor-hijo-ej1.component.html',
  styleUrls: ['./contenedor-hijo-ej1.component.css']
})
export class ContenedorHijoEj1Component implements OnInit {
  @Input() cambioColorFromPadre: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
