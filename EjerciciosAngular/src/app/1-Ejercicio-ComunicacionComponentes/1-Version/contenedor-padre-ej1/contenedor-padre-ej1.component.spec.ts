import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContenedorPadreEj1Component } from './contenedor-padre-ej1.component';

describe('ContenedorPadreEj1Component', () => {
  let component: ContenedorPadreEj1Component;
  let fixture: ComponentFixture<ContenedorPadreEj1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContenedorPadreEj1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContenedorPadreEj1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
