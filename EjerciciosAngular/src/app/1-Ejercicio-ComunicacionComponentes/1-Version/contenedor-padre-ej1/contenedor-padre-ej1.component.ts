import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'contenedor-padre-ej1',
  templateUrl: './contenedor-padre-ej1.component.html',
  styleUrls: ['./contenedor-padre-ej1.component.css']
})
export class ContenedorPadreEj1Component implements OnInit {
  btnPulsado: boolean;

  constructor() { }

  ngOnInit(): void {
    this.btnPulsado = false;
  }

  //Métodos
  cambiarColor(){
    this.btnPulsado = !this.btnPulsado;
  }
}
