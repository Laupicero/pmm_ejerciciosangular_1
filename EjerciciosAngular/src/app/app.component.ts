import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'EjerciciosAngular';
  ejercicio1Activo: boolean;
  ejercicio2Activo: boolean;
  ejercicio3Activo: boolean;
  ejercicio4Activo: boolean;

  ngOnInit(): void {
    this.ejercicio1Activo = false;
    this.ejercicio2Activo = false;
    this.ejercicio3Activo = false;
    this.ejercicio4Activo = false;
  }

  //Métodos
  mostrarEjercicioUno(){ this.ejercicio1Activo = ! this.ejercicio1Activo; }

  mostrarEjercicioDos(){ this.ejercicio2Activo = ! this.ejercicio2Activo; }

  mostrarEjercicioTres(){ this.ejercicio3Activo = ! this.ejercicio3Activo; }

  mostrarEjercicioCuatro(){ this.ejercicio4Activo = ! this.ejercicio4Activo; }
}
